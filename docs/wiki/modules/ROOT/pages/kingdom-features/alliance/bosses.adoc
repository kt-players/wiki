= Alliance Bosses
Blua Lupo

[NOTE,caption=Short version]
====
* Bosses is the main source of personal contribution, but cost a lot of Alliance Reputation.
* Bosses are a limited resource.
* The bosses available depend on the alliance level.
====


== Description

=== Gameplay

The Alliance Bosses are bosses like xref:kingdom-features/jotun.adoc[Jotun and his minions].
Players can defeat them using their heroes and earn personal contribution for each attack they deal.

If a boss is defeated, the alliance also earn Alliance Experience.


=== Rewards

* [x] *Alliance Exp*
* [ ] *Alliance Reputation*
* [x] *Personal contribution*


=== Cost

* Alliance Reputation
* or Gems


== How does it work?

=== Available bosses

The list of available bosses depends on the xref:kingdom-features/alliance/index.adoc#level[Alliance's level].
Each new level unlocks a new boss, which costs more to open but yields more rewards.


=== Opening bosses

For members to be allowed to fight an alliance boss, this boss must first be open.

* The Leader and Lords can spend Alliance Reputation or gems to open a boss.
* Elites can spend gems to open a boss.

The amount of reputation or gems depends on the boss's level.


[#damage]
=== Damaging bosses

Like xref:kingdom-features/jotun.adoc[Jotun's minions], players attack the bosses with their heroes.
// TODO link to military power page
The amount of damage is equal to the military power of that hero.

Each attack also yields the player with an amount of personal contribution directly proportional to that attack:

latexmath:[contrib = \frac{damage}{2000000}]

.Calculating the max personal contribution that can be obtained from a boss
[example]
====
The Relentless Raider has 400M life.

latexmath:[\frac{400000000}{2000000}=200], so a player could theoretically earn 200 personal contribution from slaying this boss.footnote:[Actually, it is not uncommon to earn 201 or 202 personal contribution from the Relentless Raider. This is explained through rounding issues and not a bug per se.]
====


=== Second round of level boss

Each alliance boss can be opened twice a day: when all bosses are vanquished, they reset to their non-open state.


=== Invincible Boss

After all bosses have been vanquished twice, a new Invincible boss can be open.
That boss cannot be vanquished and yields less personal contribution per damage.

[IMPORTANT]
====
* The invincible boss does not yield any Alliance Exp.

* The invincible boss's damage to personal contribution ratio is lower to that of "level bosses":
+
stem:[contrib = \frac{damage}{8000000}]
====


== Bosses recap

[options="header",cols="2h,>1,>1,>1,>1,>1"]
|===
|Boss                        |Min. Alliance lvl |Total life |Rep. opening cost |Gem opening cost | damage / contrib ratio
|Relentless Raider           |      1 |   400M |   100 |    #?# |  2M
|Relentless Raider Champion  |      1 |     1B |   200 |    #?# |  2M
|Blessed Knight              |      2 |   2.4B |   400 |    #?# |  2M
|Blessed Knight Champion     |      3 |   4.2B |   600 |    #?# |  2M
|Mercenary Captain           |      4 |   6.4B |   800 |    #?# |  2M
|Mercenary Captain Champion  |      5 |     9B |  1000 |    #?# |  2M
|Desert Warrior              |      6 |    12B |  1200 |    #?# |  2M
|Desert Warrior Champion     |      7 |  19.2B |  1400 |    #?# |  2M
|Bloodthirsty Noble          |      8 |  22.4B |  1600 |    #?# |  2M
|Bloodthirsty Noble Champion |      9 |  28.8B |  1800 |    #?# |  2M
|Ambitious Nomad             |     10 |  46.8B |  2000 |    #?# |  2M
|Ambitious Nomad Champion    |     11 |  87.8B |  2200 |    #?# |  2M
|Invincible Boss             |      1 |      ∞ |   #?# |   1800 |  [.red]*8M*
|===


== Insights

=== Reputation cost

CAUTION: An alliance cannot earn enough reputation daily to pay for opening all the bosses.

This can be somehow mitigated through xref:kingdom-features/miner-mania.adoc[] and through opening some bosses with gems.

Some alliances also have a strategy of only opening the highest level bosses, but this also means they cannot access the invincible bosses.


=== Sharing the contribution

Because bosses are a limited resource, it may be tempting for stronger players to go all out and make the most of them.
This approach however leaves smaller or less early players without a chance to profit from this source of contribution.

Many alliances set a cap on the max damage each player is allowed to make on the level bosses.
In that case, it is recommended to allow free for all past a certain time: the bosses cost a lot to open, so avoid having this expense going to waste.

Using the damage ranking in combination with the <<damage,damage to contribution ratio>>, it's possible to determine how much damage is still allowed and which heroes a player can still send based on their military power.
