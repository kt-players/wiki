= Events
Blua Lupo

Events last from 3 to 7 days depending on the event.
Some seasonal events may last for a month.

Events usually consist in one or several mini-games, sometimes in competitions with other players, some times in collaboration with your xref:kingdom-features/alliance/index.adoc[alliance].
Those mini-games usually allow you to score some kind of points, that can give you rewards in two ways:

* from rankings against other players;
* from exchanging in shops.

Many events also feature a money spending incentive by providing rewards for certain levels of xref:mechanics/vip.adoc[VIP EXP] earned.

[CAUTION,caption=To document]
====
These pages must be written.

* [ ] Artifact of the Divine Soldier
* [ ] Naval Warfare (Gangs of the Shire)
* [ ] Gamebox

… and more to come.
====